SOP of Working From Home
-----------------------------
- I am going to make a repository on Gitlab, and all of you will be added as collaborator. Make sure you all have entered your data in Employee Sheet.
- You all will be required to make a folder of your name in repository, and put python etc (whatever you are working on) in that folder by the name of date format. Like 2020-03-19.py, and will be required to put all the code of tasks in single file for each day, and push to repository via Git CLI. You can ask for method if you do not know how to do it. NO ONE WILL USE WEBSITE TO UPLOAD FILES. 
- At the end of the day, EVERYONE from Interns will be required to ask for review from ANY SENIOR employee of company.
- If the Senior Employee will give review that your work defends your whole day effort towards solving an issue, or completing a task, He will say Umair to mark that intern's attendance.


Warnings:
------------------
1. If no senior employee will give positive feedback to your work, You will be considered ABSENT. So it will be better to be in contact with a senior continuously all the day to be on right track.
2. Meanwhile Work From Home (WFH), Absent of 2 days, as per following above criteria, will cause the termination of that Intern.
3. If any intern need a half day or full day leave, get approval from any senior employee. Only if senior employee will say Umair to allow someone leave, it will be considered. No direct leave applications to Umair will be entertained.